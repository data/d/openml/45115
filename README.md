# OpenML dataset: alarm_6

https://www.openml.org/d/45115

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Alarm Bayesian Network. Sample 6.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#alarm)

- Number of nodes: 37

- Number of arcs: 46

- Number of parameters: 509

- Average Markov blanket size: 3.51

- Average degree: 2.49

- Maximum in-degree: 4

**Authors**: I. A. Beinlich, H. J. Suermondt, R. M. Chavez, and G. F. Cooper

**Please cite**: ([URL](https://doi.org/10.1007/978-3-642-93437-7_28)): I. A. Beinlich, H. J. Suermondt, R. M. Chavez, and G. F. Cooper. The ALARM Monitoring System: A Case Study with Two Probabilistic Inference Techniques for Belief Networks. In Proceedings of the 2nd European Conference on Artificial Intelligence in Medicine, pages 247-256. Springer-Verlag, 1989.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45115) of an [OpenML dataset](https://www.openml.org/d/45115). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45115/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45115/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45115/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

